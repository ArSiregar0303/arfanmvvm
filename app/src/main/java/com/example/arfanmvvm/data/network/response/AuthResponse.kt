package com.example.arfanmvvm.data.network.response

import com.example.arfanmvvm.data.db.entities.User

data class AuthResponse (
    val isSuccessful : Boolean?,
    val message : String,
    val user : User?
)