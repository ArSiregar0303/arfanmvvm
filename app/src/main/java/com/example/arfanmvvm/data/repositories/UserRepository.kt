package com.example.arfanmvvm.data.repositories

import com.example.arfanmvvm.data.network.MyApi
import com.example.arfanmvvm.data.network.response.AuthResponse
import retrofit2.Response

class UserRepository {

    suspend fun userLogin(email: String, password: String): Response<AuthResponse> {
        return MyApi()
            .userLogin(email, password)
    }
}