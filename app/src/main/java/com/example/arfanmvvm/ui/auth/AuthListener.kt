package com.example.arfanmvvm.ui.auth

import androidx.lifecycle.LiveData
import com.example.arfanmvvm.data.db.entities.User

interface AuthListener {

    fun onStarted()
    fun onSuccess(user: User)
    fun onFailure(message: String)
}